/**
 * UNIVERSIDAD DE COSTA RICA
 * EJEMPLO DE TRABAJO EN GITLAB
 * @author Geovanny Cordero Valverde
 */


public class Paciente {
    //añadir los atributos según el modelo + sets y gets y terminar el constructor
    String nombre;
    Padecimiento padecimiento;
    
    public Paciente(String nom, Padecimiento pad){
        this.nombre = nom;
        this.padecimiento = pad;
    }
    
    public String getNombre(){
        return this.nombre;
    } 
    
    public Padecimiento getPadecimiento(){
        return this.padecimiento;
    }
    
    public void setNombre(String nom){
        this.nombre = nom;
    }
    
    public void setPadecimiento(Padecimiento p){
        this.padecimiento = p;
    }
}
