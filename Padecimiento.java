/**
 * UNIVERSIDAD DE COSTA RICA
 * EJEMPLO DE TRABAJO EN GITLAB
 * @author Carlos Delgado Rojas
 */


public enum Padecimiento {

    EMERGENCIA,

    CARDIOLOGIA,

    EMERGENCIA_INTERNABLE,

    CARDIOLOGIA_OPERABLE
}
