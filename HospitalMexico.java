/**
 * UNIVERSIDAD DE COSTA RICA
 * EJEMPLO DE TRABAJO EN GITLAB
 * @author Juan Campos Obando
 */


public class HospitalMexico implements Hospital {


    private Emergencias emergencias;        // Área de emergencias que pertenece al hospital


    private Cardiologia cardiologia;        // Área de cardiología que pertenece al hospital

    public HospitalMexico(){
        this.emergencias = new Emergencias();
        this.cardiologia = new Cardiologia();
    }

    @Override
    public void recibirPaciente(Paciente p) {
        //con base en el padecimiento del paciente, decide que hacer
    }

}
