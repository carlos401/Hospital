/**
 * UNIVERSIDAD DE COSTA RICA
 * EJEMPLO DE TRABAJO EN GITLAB
 * @author Carlos Delgado Rojas
 */


public interface Hospital {
    /**
     * Método para atender pacientes en un hospital
     * @param p el paciente a atender
     */
    public void recibirPaciente(Paciente p);
}
