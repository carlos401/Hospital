/**
 * UNIVERSIDAD DE COSTA RICA
 * EJEMPLO DE TRABAJO EN GITLAB
 * @author Carlos Delgado Rojas
 */


public class Main {

    public static void main (String[] args){
        Hospital hospital = new HospitalMexico();
        hospital.recibirPaciente(new Paciente("Carlos",Padecimiento.EMERGENCIA));
    }
}
